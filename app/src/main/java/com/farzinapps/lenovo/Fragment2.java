package com.farzinapps.lenovo.viewpageroneexample;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class Fragment2 extends Fragment {
    Context context;

    public Fragment2() {
    }

    @SuppressLint("ValidFragment")
    public Fragment2(Context context) {
        this.context=context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        //Toast.makeText(context, ""+2, Toast.LENGTH_SHORT).show();
        View v = inflater.inflate(R.layout.tab2,container,false);
        return v;
    }
}
